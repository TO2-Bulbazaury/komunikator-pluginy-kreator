import Plugin.Plugin;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * Created by mk on 18/01/16.
 */
public class Main extends Application {
    public void start(Stage primaryStage) {

        try {

            Pane root = new Pane();

            Plugin p = new DemoPlugin();

            Button b = p.getPluginUI().getButton();
            Dialog d = p.getPluginUI().getDialog();

            b.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Optional result = d.showAndWait();
                    result.ifPresent(emoji -> {
                        if(emoji instanceof String){
                            System.out.println(emoji);
                        }
                    });
                }
            });


            root.getChildren().add(b);

            Scene scene = new Scene(root, 800, 400);
            primaryStage.setScene(scene);
            primaryStage.show();



        } catch(Exception e){
            e.printStackTrace();
        }

    }
    public static void main(String[] args) {

        launch(args);

    }
}
