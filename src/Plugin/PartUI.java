package Plugin;

import javafx.scene.control.Button;
import javafx.scene.control.Dialog;

/**
 * Created by mk on 17/01/16.
 */
public class PartUI {
    private Button b;
    private Dialog d;

    public PartUI(Button b, Dialog d) {
        this.b = b;
        this.d = d;
    }

    public Button getButton(){
        return b;
    }

    public Dialog getDialog(){
        return d;
    }
}
