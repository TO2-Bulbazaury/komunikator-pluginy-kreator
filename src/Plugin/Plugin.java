package Plugin;

/**
 * Created by mk on 16/12/15.
 */
public class Plugin {
    private String name;
    private String description;
    private int priority;
    private Boolean preference = false;

    public Plugin(String n, int p) {
        name = n;
        priority = p;
    }

    public String parse(String messageBody) {
        return messageBody;
    }

    public PartUI getPluginUI() {                                   //only for Messages
        //specific implementation for every plugin
        //might not be overridden (returns null)
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isPreference() {
        return preference;
    }

    public void setPreference(Boolean preference) {
        this.preference = preference;
    }

    public Boolean getPreference() {
        return preference;
    }

    public int compareTo(Plugin comparePlugin) {

        int compareQuantity = ((Plugin) comparePlugin).getPriority();

        //descending order
        return compareQuantity - this.priority;

    }

}
