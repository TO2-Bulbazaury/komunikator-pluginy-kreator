import Plugin.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mk on 18/01/16.
 */
public class DemoPlugin extends Plugin {

    private HashMap<String, Pair<String, Image>> emoticons;

    public DemoPlugin() {
        super("Demo", 1);

        this.emoticons = new HashMap<>();
        setEmoticons();
    }

    private void setEmoticons(){
        emoticons.put(":D", new Pair("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/png/1f600.png", new Image("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/png/1f600.png")));
        emoticons.put(";(", new Pair("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/png/1f62d.png", new Image("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/png/1f62d.png")));
    }

    public String parse(String messageBody){

        for(Map.Entry<String, Pair<String, Image>> emoticon : emoticons.entrySet()){
            messageBody = messageBody.replaceAll(emoticon.getKey(), "<img src=\""+emoticon.getValue().getKey()+" style=\"font-size: 0.625em;\">");
        }
        return messageBody;
    }

    public PartUI getPluginUI(){

        Dialog dialog = new Dialog();
        dialog.setTitle("Choose emoticon");

        int row = 1, column = 1, columnLimit = 4;


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        for(Map.Entry<String, Pair<String, Image>> emoji : this.emoticons.entrySet()){
            Button b = new Button("", new ImageView(emoji.getValue().getValue()));

            b.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.setResult(emoji.getKey());
                    dialog.close();
                }
            });

            grid.add(b, column, row);
            column++;
            if(column > columnLimit){
                column = 1;
                row++;
            }
        }


        dialog.getDialogPane().setContent(grid);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);

        Button button = new Button(":)");

        return new PartUI(button, dialog);

    }

}
