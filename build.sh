#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Specify main plugin class name"
    exit 1
fi

if [ ! -f "src/$1.java" ]; then
    echo "File $1.java does not exists"
    exit 1
fi

echo "Building jar"
cd out/production/komunikator-pluginy-kreator
jar cvf "../../../$1.jar" "$1.class"